#include"../../../include/fast_io.h"

extern std::size_t my_assembly_function() noexcept;

int main()
{
	println(fast_io::out(),"result from my_assembly_function() is ",my_assembly_function());
}