#include"../../../include/fast_io.h"

int main()
{
	std::size_t result;
	__asm__(R"(
		mov $224124121241, %[reg1]
	)":[reg1]"=r"(result));

	println(fast_io::out(),"result from inline assembly is ",result);
}