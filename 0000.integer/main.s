	.file	"main.cc"
	.text
	.def	__main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
	.align 8
.LC0:
	.ascii "result from my_assembly_function() is \0"
	.section	.text.unlikely,"x"
.LCOLDB1:
	.section	.text.startup,"x"
.LHOTB1:
	.globl	main
	.def	main;	.scl	2;	.type	32;	.endef
	.seh_proc	main
main:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$248, %rsp
	.seh_stackalloc	248
	.seh_endprologue
	call	__main
	call	_Z20my_assembly_functionv
	movq	%rax, %rbx
	movl	$-11, %ecx
	call	GetStdHandle
	movq	%rax, %r12
	leaq	.LC0(%rip), %rax
	movq	%rax, 192(%rsp)
	movq	$38, 200(%rsp)
	movabsq	$-8446744073709551617, %rax
	cmpq	%rax, %rbx
	ja	.L2
	movabsq	$999999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L21
	movabsq	$99999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L22
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L23
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L24
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L25
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L26
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L27
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L28
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L29
	cmpq	$999999999, %rbx
	ja	.L30
	cmpq	$99999999, %rbx
	ja	.L31
	cmpq	$9999999, %rbx
	ja	.L32
	cmpq	$999999, %rbx
	ja	.L33
	cmpq	$99999, %rbx
	ja	.L34
	cmpq	$9999, %rbx
	ja	.L35
	cmpq	$999, %rbx
	ja	.L36
	cmpq	$99, %rbx
	ja	.L4
	cmpq	$10, %rbx
	sbbl	%eax, %eax
	leal	2(%rax), %r11d
	leaq	96(%rsp), %rsi
	leaq	(%rsi,%r11), %r8
.L7:
	cmpq	$9, %rbx
	jbe	.L9
.L50:
	leaq	_ZN7fast_io7details37shared_inline_constexpr_base_table_tbIDuLy10ELb0ELb0EEE(%rip), %rax
	movzwl	(%rax,%rbx,2), %eax
	movw	%ax, -2(%r8)
.L10:
	movq	%rsi, 208(%rsp)
	movq	%r11, 216(%rsp)
	movb	$10, 95(%rsp)
	leaq	95(%rsp), %rax
	movq	%rax, 224(%rsp)
	movq	$1, 232(%rsp)
	movq	$0, 128(%rsp)
	movq	$0, 136(%rsp)
	movq	$0, 144(%rsp)
	movq	$0, 152(%rsp)
	leaq	128(%rsp), %rax
	movq	%rax, 40(%rsp)
	movl	$-1, 32(%rsp)
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r12, %rcx
	call	LockFileEx
	testl	%eax, %eax
	movq	$-1, %rax
	cmovne	%r12, %rax
	movq	%rax, 72(%rsp)
	leaq	192(%rsp), %rax
	movq	%rax, 48(%rsp)
	leaq	240(%rsp), %rax
	movq	%rax, 64(%rsp)
	leaq	160(%rsp), %r15
.L13:
	movq	48(%rsp), %rax
	movq	8(%rax), %rsi
	movq	%rsi, 56(%rsp)
	movq	(%rax), %rdi
	testq	%rsi, %rsi
	je	.L18
	movq	%rsi, %r14
	xorl	%esi, %esi
	movl	$-1, %ebp
	jmp	.L12
.L49:
	subq	%r13, %r14
	je	.L17
.L12:
	movl	$4294967294, %eax
	cmpq	%rax, %r14
	movl	%ebp, %ebx
	cmovbe	%r14d, %ebx
	movl	$4294967295, %r13d
	cmovbe	%r14, %r13
	movl	$0, 160(%rsp)
	movq	$0, 32(%rsp)
	movq	%r15, %r9
	movl	%ebx, %r8d
	movq	%rdi, %rdx
	movq	%r12, %rcx
	call	WriteFile
	testl	%eax, %eax
	je	.L47
	movl	160(%rsp), %edx
	addq	%rdx, %rsi
	cmpl	%ebx, %edx
	jnb	.L49
.L17:
	cmpq	%rsi, 56(%rsp)
	jb	.L14
.L18:
	addq	$16, 48(%rsp)
	movq	48(%rsp), %rax
	cmpq	%rax, 64(%rsp)
	jne	.L13
.L14:
	movq	72(%rsp), %rsi
	cmpq	$-1, %rsi
	je	.L41
	leaq	160(%rsp), %rdi
	movl	$8, %ecx
	xorl	%eax, %eax
	rep stosl
	leaq	160(%rsp), %rax
	movq	%rax, 32(%rsp)
	orl	$-1, %r9d
	orl	$-1, %r8d
	xorl	%edx, %edx
	movq	%rsi, %rcx
	call	UnlockFileEx
.L41:
	xorl	%eax, %eax
	addq	$248, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L2:
	leaq	116(%rsp), %r8
	movl	$20, %r11d
	leaq	96(%rsp), %rsi
.L20:
	leaq	_ZN7fast_io7details37shared_inline_constexpr_base_table_tbIDuLy10ELb0ELb0EEE(%rip), %r10
	movabsq	$2951479051793528259, %r9
.L8:
	movq	%rbx, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r9
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %rcx
	salq	$2, %rcx
	movq	%rbx, %rdx
	subq	%rcx, %rdx
	movq	%rbx, %rcx
	movq	%rax, %rbx
	subq	$2, %r8
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r8)
	cmpq	$9999, %rcx
	ja	.L8
	cmpq	$9, %rbx
	ja	.L50
.L9:
	addl	$48, %ebx
	movb	%bl, -1(%r8)
	jmp	.L10
.L22:
	movl	$18, %r11d
.L3:
	leaq	96(%rsp), %rsi
	leaq	96(%rsp,%r11), %r8
	cmpq	$99, %rbx
	ja	.L20
	jmp	.L7
.L21:
	movl	$19, %r11d
	jmp	.L3
.L23:
	movl	$17, %r11d
	jmp	.L3
.L24:
	movl	$16, %r11d
	jmp	.L3
.L25:
	movl	$15, %r11d
	jmp	.L3
.L26:
	movl	$14, %r11d
	jmp	.L3
.L32:
	movl	$8, %r11d
	jmp	.L3
.L27:
	movl	$13, %r11d
	jmp	.L3
.L28:
	movl	$12, %r11d
	jmp	.L3
.L29:
	movl	$11, %r11d
	jmp	.L3
.L30:
	movl	$10, %r11d
	jmp	.L3
.L31:
	movl	$9, %r11d
	jmp	.L3
.L33:
	movl	$7, %r11d
	jmp	.L3
.L34:
	movl	$6, %r11d
	jmp	.L3
.L36:
	movl	$4, %r11d
	jmp	.L3
.L35:
	movl	$5, %r11d
	jmp	.L3
.L4:
	movl	$100, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %rbx
	leaq	_ZN7fast_io7details37shared_inline_constexpr_base_table_tbIDuLy10ELb0ELb0EEE(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	movw	%ax, 97(%rsp)
	leaq	97(%rsp), %r8
	movl	$3, %r11d
	leaq	96(%rsp), %rsi
	jmp	.L9
	.seh_endproc
	.section	.text.unlikely,"x"
	.def	main.cold;	.scl	3;	.type	32;	.endef
	.seh_proc	main.cold
	.seh_stackalloc	312
	.seh_savereg	%rbx, 248
	.seh_savereg	%rsi, 256
	.seh_savereg	%rdi, 264
	.seh_savereg	%rbp, 272
	.seh_savereg	%r12, 280
	.seh_savereg	%r13, 288
	.seh_savereg	%r14, 296
	.seh_savereg	%r15, 304
	.seh_endprologue
main.cold:
.L47:
	ud2
	.section	.text.startup,"x"
	.section	.text.unlikely,"x"
	.seh_endproc
.LCOLDE1:
	.section	.text.startup,"x"
.LHOTE1:
	.globl	_ZN7fast_io7details37shared_inline_constexpr_base_table_tbIDuLy10ELb0ELb0EEE
	.section	.rdata$_ZN7fast_io7details37shared_inline_constexpr_base_table_tbIDuLy10ELb0ELb0EEE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details37shared_inline_constexpr_base_table_tbIDuLy10ELb0ELb0EEE:
	.byte	48
	.byte	48
	.byte	48
	.byte	49
	.byte	48
	.byte	50
	.byte	48
	.byte	51
	.byte	48
	.byte	52
	.byte	48
	.byte	53
	.byte	48
	.byte	54
	.byte	48
	.byte	55
	.byte	48
	.byte	56
	.byte	48
	.byte	57
	.byte	49
	.byte	48
	.byte	49
	.byte	49
	.byte	49
	.byte	50
	.byte	49
	.byte	51
	.byte	49
	.byte	52
	.byte	49
	.byte	53
	.byte	49
	.byte	54
	.byte	49
	.byte	55
	.byte	49
	.byte	56
	.byte	49
	.byte	57
	.byte	50
	.byte	48
	.byte	50
	.byte	49
	.byte	50
	.byte	50
	.byte	50
	.byte	51
	.byte	50
	.byte	52
	.byte	50
	.byte	53
	.byte	50
	.byte	54
	.byte	50
	.byte	55
	.byte	50
	.byte	56
	.byte	50
	.byte	57
	.byte	51
	.byte	48
	.byte	51
	.byte	49
	.byte	51
	.byte	50
	.byte	51
	.byte	51
	.byte	51
	.byte	52
	.byte	51
	.byte	53
	.byte	51
	.byte	54
	.byte	51
	.byte	55
	.byte	51
	.byte	56
	.byte	51
	.byte	57
	.byte	52
	.byte	48
	.byte	52
	.byte	49
	.byte	52
	.byte	50
	.byte	52
	.byte	51
	.byte	52
	.byte	52
	.byte	52
	.byte	53
	.byte	52
	.byte	54
	.byte	52
	.byte	55
	.byte	52
	.byte	56
	.byte	52
	.byte	57
	.byte	53
	.byte	48
	.byte	53
	.byte	49
	.byte	53
	.byte	50
	.byte	53
	.byte	51
	.byte	53
	.byte	52
	.byte	53
	.byte	53
	.byte	53
	.byte	54
	.byte	53
	.byte	55
	.byte	53
	.byte	56
	.byte	53
	.byte	57
	.byte	54
	.byte	48
	.byte	54
	.byte	49
	.byte	54
	.byte	50
	.byte	54
	.byte	51
	.byte	54
	.byte	52
	.byte	54
	.byte	53
	.byte	54
	.byte	54
	.byte	54
	.byte	55
	.byte	54
	.byte	56
	.byte	54
	.byte	57
	.byte	55
	.byte	48
	.byte	55
	.byte	49
	.byte	55
	.byte	50
	.byte	55
	.byte	51
	.byte	55
	.byte	52
	.byte	55
	.byte	53
	.byte	55
	.byte	54
	.byte	55
	.byte	55
	.byte	55
	.byte	56
	.byte	55
	.byte	57
	.byte	56
	.byte	48
	.byte	56
	.byte	49
	.byte	56
	.byte	50
	.byte	56
	.byte	51
	.byte	56
	.byte	52
	.byte	56
	.byte	53
	.byte	56
	.byte	54
	.byte	56
	.byte	55
	.byte	56
	.byte	56
	.byte	56
	.byte	57
	.byte	57
	.byte	48
	.byte	57
	.byte	49
	.byte	57
	.byte	50
	.byte	57
	.byte	51
	.byte	57
	.byte	52
	.byte	57
	.byte	53
	.byte	57
	.byte	54
	.byte	57
	.byte	55
	.byte	57
	.byte	56
	.byte	57
	.byte	57
	.ident	"GCC: (GCC with MCF thread model, built by cqwrteur.) 11.0.1 20210226 (experimental)"
	.def	_Z20my_assembly_functionv;	.scl	2;	.type	32;	.endef
	.def	GetStdHandle;	.scl	2;	.type	32;	.endef
	.def	LockFileEx;	.scl	2;	.type	32;	.endef
	.def	WriteFile;	.scl	2;	.type	32;	.endef
	.def	UnlockFileEx;	.scl	2;	.type	32;	.endef
